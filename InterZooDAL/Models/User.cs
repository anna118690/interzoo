﻿namespace InterZooDAL.Models
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;

    public partial class User : IEntity<int>
        

    {
        private int _idUser;
        private string _nom;
        private string _email;
        private string _motDePasse;
        private int _idFormule;
        private int _idAnimal;
        private int _idTravail;

        public int IdUser
        {
            get { return _idUser; }
            set { _idUser = value; }
        }

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }


        public string MotDePasse
        {
            get { return _motDePasse; }
            set { _motDePasse = value; }
        }

        public int IdFormule
        {
            get { return _idFormule; }
            set { _idFormule = value; }
        }

        public int IdAnimal
        {
            get { return _idAnimal; }
            set { _idAnimal = value; }
        }


        public int IdTravail
        {
            get { return _idTravail; }
            set { _idTravail = value; }
        }

        public string HashMDP
        {
            get
            {
                if (_motDePasse == null) throw new InvalidOperationException("Le mot de passe est vide");
                using (SHA512 sha512Hash = SHA512.Create())
                {

                    byte[] sourceBytes = Encoding.UTF8.GetBytes(_motDePasse);
                    byte[] hashBytes = sha512Hash.ComputeHash(sourceBytes);
                    string hash = BitConverter.ToString(hashBytes).Replace("-", String.Empty);
                    return hash;
                }
            }
        }







        public Formule Formule { get; set;  }

        public Animal Animal { get; set; }

        public Travail Travail { get; set; }

        public int Id
        {
            get
            {
                return IdUser;
            }
        }






    }
}

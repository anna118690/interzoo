﻿

namespace InterZooDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;

    public partial class Espece : IEntity<int>
    {

        private int _idEspece;
        private string _nom;


        public int IdEspece
        {
            get { return _idEspece; }
            set { _idEspece = value; }
        }


        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public IEnumerable<Animal> Animal { get; set; }

        public IEnumerable<EspeceRegion> EspeceRegion { get; set; }

        public int Id
        {
            get
            {
                return IdEspece;
            }
        }

        public Espece()
        {
            this.Animal = new List<Animal>();
            this.EspeceRegion = new List<EspeceRegion>();  
        }

    }
}

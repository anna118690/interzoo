﻿
namespace InterZooDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;
    using InterZooDAL.Infra;

    public partial class TravailAnimal : IEntity<CompositeKey<int, int>>
    {

        private int _idTravail;
        private int _idAnimal;
        private string _description;

        public int IdTravail
        {
            get { return _idTravail; }
            set { _idTravail = value; }
        }


        public int IdAnimal
        {
            get { return _idAnimal; }
            set { _idAnimal = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public Travail Travail { get; set; }

        public Animal Animal { get; set; }

        public CompositeKey<int, int> Id
        {
            get
            {
                return new CompositeKey<int, int>() { PK1 = IdAnimal, PK2 = IdTravail };
            }
        }



    }
}

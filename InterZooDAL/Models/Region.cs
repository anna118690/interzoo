﻿namespace InterZooDAL.Models
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;

    public partial class Region : IEntity<int>
    {

        private int _idRegion;
        private string _nom;


        public int IdRegion
        {
            get { return _idRegion; }
            set { _idRegion = value; }
        }

        

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public IEnumerable<Animal> Animal { get; set; }

        public IEnumerable<EspeceRegion> EspeceRegion { get; set; }

        public int Id
        {
            get
            {
                return IdRegion;
            }
        }

        public Region()
        {
            this.Animal = new List<Animal>();
            this.EspeceRegion = new List<EspeceRegion>();
        }

    }
}

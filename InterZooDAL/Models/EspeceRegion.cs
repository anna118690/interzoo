﻿

namespace InterZooDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;
    using InterZooDAL.Infra;


    public partial class EspeceRegion : IEntity<CompositeKey<int, int>>
    {
        private int _idRegion;
        private int _idEspece;

        public int IdRegion
        {
            get { return _idRegion; }
            set { _idRegion = value; }
        }



        public int IdEspece
        {
            get { return _idEspece; }
            set { _idEspece = value; }
        }

        public Region Region { get; set; }

        public Espece Espece { get; set; }

        public CompositeKey<int, int> Id
        {
            get
            {
                return new CompositeKey<int, int>() { PK1 = IdRegion, PK2 = IdEspece };
            }
        }


    }
}

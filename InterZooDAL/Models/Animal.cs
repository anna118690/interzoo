﻿namespace InterZooDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;
    public partial class Animal : IEntity<int>
    {
        private int _idAnimal;
        private string _nom;
        private DateTime _dateDeNaissance;
        private DateTime _dateDeDecede;
        private int _idRegion;
        private int _idEspece;


        public int IdAnimal
        {
            get { return _idAnimal; }
            set { _idAnimal = value; }
        }


        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        

        public DateTime DateDeNaissance
        {
            get { return _dateDeNaissance; }
            set { _dateDeNaissance = value; }
        }




        public DateTime DateDeDecede
        {
            get { return _dateDeDecede; }
            set { _dateDeDecede = value; }
        }

 
        public int IdRegion 
        {
            get { return _idRegion; }
            set { _idRegion = value; }
        }


        public int IdEspece
        {
            get { return _idEspece; }
            set { _idEspece = value; }
        }



        public IEnumerable<User> User { get; set; }

        public IEnumerable<TravailAnimal> TravailAnimal { get; set; }


        public Region Region { get; set; }

        public Espece Espece { get; set; }

        public int Id
        {
            get
            {
                return IdAnimal;
            }
        }

        public Animal()
        {
            this.TravailAnimal = new List<TravailAnimal>();
        }



    }
}

﻿

namespace InterZooDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;


    public partial class Formule : IEntity<int>
    {
        private int _idFormule;
        private string _nom;
        private double _prix;
        private DateTime _dateDeDebut;
        private DateTime _dateDeExpiration;

        public int IdFormule
        {
            get { return _idFormule; }
            set { _idFormule = value; }
        }

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public double Prix
        {
            get { return _prix; }
            set { _prix = value; }
        }

        public DateTime DateDeDebut
        {
            get { return _dateDeDebut; }
            set { _dateDeDebut = value; }
        }
       
        public DateTime DateDeExpiration
        {
            get { return _dateDeExpiration; }
            set { _dateDeExpiration = value; }
        }

        public IEnumerable<User> User { get; set; }

        public int Id
        {
            get
            {
                return IdFormule;
            }
        }

        public Formule()
        { this.User = new List<User>();
        }



    }
}

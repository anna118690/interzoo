﻿

namespace InterZooDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using InterZooDAL.Interface;

    public partial class Travail : IEntity<int>
    {
        private int _idTravail;
        private string _nom;
        private string _typeDeConcrat;
        private DateTime _dateDeDebut;
        private DateTime _dateDeExpiration;


        public int IdTravail
        {
            get { return _idTravail; }
            set { _idTravail = value; }
        }

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string TypeDeConcrat
        {
            get { return _typeDeConcrat; }
            set { _typeDeConcrat = value; }
        }

        public DateTime DateDeDebut
        {
            get { return _dateDeDebut; }
            set { _dateDeDebut = value; }
        }



        public DateTime DateDeExpiration
        {
            get { return _dateDeExpiration; }
            set { _dateDeExpiration = value; }
        }

        public IEnumerable<User> User { get; set; }

        public IEnumerable<TravailAnimal>TravailAnimal { get; set; }

        public int Id
        {
            get
            {
                return IdTravail;
            }
        }

        public Travail()
        {
            this.User = new List<User>();
            this.TravailAnimal = new List<TravailAnimal>();
        }

    }
}

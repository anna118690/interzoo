﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterZooDAL.Interface
{
    public interface IEntity<TKey>
    {
        TKey Id { get; }
    }
}

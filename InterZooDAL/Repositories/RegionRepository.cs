﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class RegionRepository : BaseRepository<Region, int>
    {

        public RegionRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = "Select * from Region where idRegion=@idRegion";
            SelectAllCommand = "Select * from Region";
            InsertCommand = @"INSERT INTO  Region(Nom)
                            OUTPUT inserted.idRegion VALUES(@Nom)";
            UpdateCommand = @"UPDATE  Region
                            SET Nom = @Nom>
                            WHERE IdRegion = @IdRegion;";
            DeleteCommand = @"Delete from  Region  WHERE IdRegion = @IdRegion;";
        }


        public override IEnumerable<Region> GetAll()
        {
            return base.getAll(Map);
        }

        public override Region GetOne(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdRegion", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Region Insert(Region toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdRegion = id;
            return toInsert;
        }

        public override bool Update(Region toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdRegion", id);
            return base.Delete(QueryParameters);
        }


        #region Mappers

        private Dictionary<string, object> MapToDico(Region toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdRegion"] = toInsert.IdRegion;
            p["Nom"] = toInsert.Nom;
            return p;
        }

        private Region Map(SqlDataReader p)
        {
            return new Region()
            {
                IdRegion = (int)p["IdRegion"],
                Nom = p["Nom"].ToString(),

            };
        }
        #endregion
    }
}

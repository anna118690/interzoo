﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class EspeceRepository : BaseRepository<Espece, int>
    {
        public EspeceRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = "Select * from Espece where idEspece=@idEspece";
            SelectAllCommand = "Select * from Espece";
            InsertCommand = @"INSERT INTO  Espece(Nom)
                            OUTPUT inserted.idEspece VALUES(@Nom)";
            UpdateCommand = @"UPDATE  Espece
                            SET Nom = @Nom>
                            WHERE IdEspece = @IdEspece;";
            DeleteCommand = @"Delete from  Espece  WHERE IdEspece = @IdEspece;";
        }


        public override IEnumerable<Espece> GetAll()
        {
            return base.getAll(Map);
        }

        public override Espece GetOne(int id)
        {
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            Parameters.Add("IdEspece", id);
            return base.getOne(Map, Parameters);
        }

        public override Espece Insert(Espece toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdEspece = id;
            return toInsert;
        }

        public override bool Update(Espece toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            Parameters.Add("IdEspece", id);
            return base.Delete(Parameters);
        }




        #region Mappers

        private Dictionary<string, object> MapToDico(Espece toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdEspece"] = toInsert.IdEspece;
            p["Nom"] = toInsert.Nom;
            return p;
        }
      
        private Espece Map(SqlDataReader p)
        {
            return new Espece()
            {
                IdEspece = (int)p["IdEspece"],
                Nom = p["Nom"].ToString(),
                
            };
        }
        #endregion


    }
}

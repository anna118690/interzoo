﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class UserRepository : BaseRepository<User, int>
    {
        public UserRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = "Select * from [User] where idUser=@idUser";
            SelectAllCommand = "Select * from [User]";
            InsertCommand = @"INSERT INTO  [User](Nom, Email, MotDePasse, IdFormule, IdAnimal, IdTravail)
                            OUTPUT inserted.idUser VALUES(@Nom,  @Email, @MotDePasse @IdFormule ,@IdAnimal, @IdTravail)";
            UpdateCommand = @"UPDATE  [User]
                            SET Nom = @Nom, Email = @Email, IdMotDePasse=@MotDePasse, IdFormule=@IdFormule, IdAnimal=@IdAnimal, IdTravail=@IdTravail >
                            WHERE IdUser = @IdUser;";
            DeleteCommand = @"Delete from  [User]  WHERE IdUser = @IdUser;";
        }


        public User VerifLogin(User user)
        {
            SelectOneCommand = "Select * from [User] where Email=@Email and MotDePasse=@MotDePasse";
            return base.getOne(Map, MapToDico(user));
        }


        public override IEnumerable<User> GetAll()
        {
            return base.getAll(Map);
        }

        public override User GetOne(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdUser", id);
            return base.getOne(Map, QueryParameters);
        }

        public override User Insert(User toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdUser = id;
            return toInsert;
        }

        public override bool Update(User toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdUser", id);
            return base.Delete(QueryParameters);
        }


        #region Mappers


        private Dictionary<string, object> MapToDico(User toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdUser"] = toInsert.IdUser;
            p["Nom"] = toInsert.Nom;
            p["Email"] = toInsert.Email;
            p["MotDePasse"] = toInsert.HashMDP;
            p["IdAnimal"] = toInsert.IdAnimal;
            p["IdFormule"] = toInsert.IdFormule;
            p["IdTravail"] = toInsert.IdTravail;
            return p;
        }


        private User Map(SqlDataReader p)
        {
            return new User()
            {
                IdUser = (int)p["IdUser"],
                Nom = p["Nom"].ToString(),
                Email = p["Email"].ToString(),
                IdFormule = (int)p["IdFormule"],
                IdAnimal = (int)p["IdAnimal"],
                IdTravail = (int)p["IdTravail"]
            };
        }
        #endregion
    }
}

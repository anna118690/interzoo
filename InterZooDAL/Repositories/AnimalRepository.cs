﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class AnimalRepository : BaseRepository<Animal, int>
    {
        public AnimalRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = "Select * from Animal where idAnimal=@idAnimal";
            SelectAllCommand = "Select * from Animal";
            InsertCommand = @"INSERT INTO  Animal(Nom, DateDeNaissance, DateDeDecede, IdRegion, IdEspace )
                            OUTPUT inserted.idAnimal VALUES(@Nom, @DateDeNaissance, @DateDeDecede, @IdRegion, @IdEspace)";
            UpdateCommand = @"UPDATE  Animal
                            SET Nom = @Nom,  DateDeNaissance = @DateDeNaissance,  DateDeDecede = @DateDeDecede, IdRegion=@IdRegion, IdEspace=@IdEspace>
                            WHERE IdAnimal = @IdAnimal;";
            DeleteCommand = @"Delete from  Animal  WHERE IdAnimal = @IdAnimal;";
        }

        public override IEnumerable<Animal> GetAll()
        {
            return base.getAll(Map);
        }

        public override Animal GetOne(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdAnimal", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Animal Insert(Animal toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdAnimal = id;
            return toInsert;
        }

        public override bool Update(Animal toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdAnimal", id);
            return base.Delete(QueryParameters);
        }



        


             public IEnumerable<Animal> GetAnimalFromUser(int idUser)
        {
            SelectAllCommand = @"SELECT       Animal.*
             FROM            Animal INNER JOIN
                         [User] ON [User].IdUser = Animal.IdUser 
             WHERE        ([User].IdUser = @IdUser) ";

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdUser", idUser);
            return base.getAll(Map, QueryParameters);
        }




        #region Mappers


        private Dictionary<string, object> MapToDico(Animal toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdAnimal"] = toInsert.IdAnimal;
            p["Nom"] = toInsert.Nom;
            p["IdRegion"] = toInsert.IdRegion;
            p["IdEspece"] = toInsert.IdEspece;
            p["DateDeNaissance"] = toInsert.DateDeNaissance;
            p["DateDeDecede"] = toInsert.DateDeDecede;
            return p;
        }


        private Animal Map(SqlDataReader p)
        {
            return new Animal()
            {
                IdAnimal = (int)p["IdAnimal"],
                Nom = p["Nom"].ToString(),
                IdRegion = (int)p["IdRegion"],
                IdEspece = (int)p["IdEspece"],
                DateDeNaissance = (DateTime)p["DateDeNaissance"],
                DateDeDecede = (DateTime)p["DateDeDecede"]
            };
        }
        #endregion


    }
}

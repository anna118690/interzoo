﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Infra;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class EspeceRegionRepository : BaseRepository<EspeceRegion, CompositeKey<int, int>>
    {
        public EspeceRegionRepository(string Cnstr) : base(Cnstr)
        {
            SelectAllCommand = "SELECT * FROM EspeceRegion";
            SelectOneCommand = "SELECT * FROM EspeceRegion WHERE IdRegion=@IdRegion AND IdEspece=@IdEspece ";
            InsertCommand = "INSERT INTO EspeceRegion(IdRegion,IdEspece) VALUES (@IdRegion,@IdEspece)";
            UpdateCommand = "UPDATE EspeceRegion SET IdRegion=@IdRegion, IdEspece=@IdEspece WHERE IdRegion=@IdRegion AND IdEspece=@IdEspece";
            DeleteCommand = "DELETE FROM  EspeceRegion WHERE IdRegion=@IdRegion AND IdEspece=@IdEspece";
        }

        public override IEnumerable<EspeceRegion> GetAll()
        {
            return base.getAll(Map);
        }

        public override EspeceRegion GetOne(CompositeKey<int, int> id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdRegion", id.PK1);
            QueryParameters.Add("IdEspece", id.PK2);
            return base.getOne(Map, QueryParameters);
        }

        public override EspeceRegion Insert(EspeceRegion toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            base.Insert(Parameters);
            return toInsert;
        }

        public override bool Update(EspeceRegion toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);

        }

        public override bool Delete(CompositeKey<int, int> id)
        {
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            Parameters.Add("IdEspece", id.PK1);
            Parameters.Add("IdRegion", id.PK2);
            return base.Delete(Parameters);
        }

        #region Mappers
        private Dictionary<string, object> MapToDico(EspeceRegion toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdEspece"] = toInsert.IdEspece;
            p["IdRegion"] = toInsert.IdRegion;
            
            return p;
        }

        private EspeceRegion Map(SqlDataReader p)
        {
            return new EspeceRegion()
            {

                IdEspece = (int)p["IdEspece"],
                IdRegion = (int)p["IdRegion"],
 
            };
        }
        #endregion




    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class FormuleRepository : BaseRepository<Formule, int>
    {
        public FormuleRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = "Select * from Formule where idFormule=@idFormule";
            SelectAllCommand = "Select * from Formule";
            InsertCommand = @"INSERT INTO  Formule(Nom, Prix, DateDeDebut, DateDeExpiration )
                            OUTPUT inserted.idFormule VALUES(@Nom, @Prix, @DateDeDebut, @DateDeExpiration)";
            UpdateCommand = @"UPDATE  Formule
                            SET Nom = @Nom,  Prix = @Prix,  DateDeDebut = @DateDeDebut, DateDeExpiration=@DateDeExpiration>
                            WHERE IdFormule = @IdFormule;";
            DeleteCommand = @"Delete from  Formule  WHERE IdFormule = @IdFormule;";
        }

        public override IEnumerable<Formule> GetAll()
        {
            return base.getAll(Map);
        }

        public override Formule GetOne(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdFormule", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Formule Insert(Formule toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdFormule = id;
            return toInsert;
        }

        public override bool Update(Formule toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdFormule", id);
            return base.Delete(QueryParameters);
        }


        #region Mappers


        private Dictionary<string, object> MapToDico(Formule toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdFormule"] = toInsert.IdFormule;
            p["Nom"] = toInsert.Nom;
            p["Prix"] = toInsert.Prix;
            p["DateDeDebut"] = toInsert.DateDeDebut;
            p["DateDeExpiration"] = toInsert.DateDeExpiration;
            return p;
        }


        private Formule Map(SqlDataReader p)
        {
            return new Formule()
            {
                IdFormule = (int)p["IdFormule"],
                Nom = p["Nom"].ToString(),
                Prix = (double)p["Prix"],
                DateDeDebut = (DateTime)p["DateDeDebut"],
                DateDeExpiration = (DateTime)p["DateDeExpiration"]
            };
        }
        #endregion

    }
}

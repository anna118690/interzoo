﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;

namespace InterZooDAL.Repositories
{
    public class TravailRepository : BaseRepository<Travail, int>
    {
        public TravailRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = "Select * from Travail where idTravail=@idTravail";
            SelectAllCommand = "Select * from Travail";
            InsertCommand = @"INSERT INTO  Travail(Nom, TypeDeConcrat, DateDeDebut, DateDeExpiration )
                            OUTPUT inserted.idAnimal VALUES(@Nom, @TypeDeConcrat, @DateDeDebut, @DateDeExpiration)";
            UpdateCommand = @"UPDATE  Travail
                            SET Nom = @Nom,  TypeDeConcrat = @TypeDeConcrat,  DateDeDebut = @DateDeDebut, DateDeExpiration=@DateDeExpiration>
                            WHERE IdTravail = @IdTravail;";
            DeleteCommand = @"Delete from  Travail  WHERE IdTravail = @IdTravail;";
        }

        public override IEnumerable<Travail> GetAll()
        {
            return base.getAll(Map);
        }

        public override Travail GetOne(int id)
        {
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            Parameters.Add("IdTravail", id);
            return base.getOne(Map, Parameters);
        }

        public override Travail Insert(Travail toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdTravail = id;
            return toInsert;
        }

        public override bool Update(Travail toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            Parameters.Add("IdTravail", id);
            return base.Delete(Parameters);
        }


        #region Mappers

        private Dictionary<string, object> MapToDico(Travail toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdTravail"] = toInsert.IdTravail;
            p["Nom"] = toInsert.Nom;
            p["TypeDeConcrat"] = toInsert.TypeDeConcrat;
            p["DateDeDebut"] = toInsert.DateDeDebut;
            p["DateDeExpiration"] = toInsert.DateDeExpiration;
            return p;
        }

        private Travail Map(SqlDataReader p)
        {
            return new Travail()
            {
                IdTravail = (int)p["IdTravail"],
                Nom = p["Nom"].ToString(),
                TypeDeConcrat = p["TypeDeConcrat"].ToString(),
                DateDeDebut = (DateTime)p["DateDeDebut"],
                DateDeExpiration = (DateTime)p["DateDeExpiration"]
            };
        }
        #endregion






    }
}

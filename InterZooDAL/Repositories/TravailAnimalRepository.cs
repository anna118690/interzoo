﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterZooDAL.Models;
using InterZooDAL.Infra;

namespace InterZooDAL.Repositories
{
    public class TravailAnimalRepository : BaseRepository<TravailAnimal, CompositeKey<int, int>>
    {
        public TravailAnimalRepository(string Cnstr) : base(Cnstr)
        {
            SelectAllCommand = "SELECT * FROM TravailAnimal";
            SelectOneCommand = "SELECT * FROM TravailAnimal WHERE IdTravail=@IdTravail AND IdAnimal=@IdAnimal ";
            InsertCommand = "INSERT INTO TravailAnimal(IdTravail,IdAnimal,Description) VALUES (@IdTravail, @IdAnimal, @Description)";
            UpdateCommand = "UPDATE TravailAnimal SET IdTravail=@IdTravail, IdAnimal=@IdAnimal,Description=@Description WHERE IdTravail=@IdTravail AND IdAnimal=@IdAnimal";
            DeleteCommand = "DELETE FROM  TravailAnimal WHERE IdTravail=@IdTravail AND IdAnimal=@IdAnimal";
        }

        public override IEnumerable<TravailAnimal> GetAll()
        {
            return base.getAll(Map);
        }

        public override TravailAnimal GetOne(CompositeKey<int, int> id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("IdTravail", id.PK1);
            QueryParameters.Add("IdAnimal", id.PK2);
            return base.getOne(Map, QueryParameters);
        }

        public override TravailAnimal Insert(TravailAnimal toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            base.Insert(Parameters);
            return toInsert;
        }

        public override bool Update(TravailAnimal toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);

        }

        public override bool Delete(CompositeKey<int, int> id)
        {
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            Parameters.Add("IdAnimal", id.PK1);
            Parameters.Add("IdRegion", id.PK2);
            return base.Delete(Parameters);
        }

        #region Mappers
        private Dictionary<string, object> MapToDico(TravailAnimal toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdTravail"] = toInsert.IdTravail;
            p["IdAnimal"] = toInsert.IdAnimal;

            return p;
        }

        private TravailAnimal Map(SqlDataReader p)
        {
            return new TravailAnimal()
            {

                IdAnimal = (int)p["IdAnimal"],
                IdTravail = (int)p["IdTravail"],

            };
        }
        #endregion



    }
}

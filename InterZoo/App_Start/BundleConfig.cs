﻿using System.Web;
using System.Web.Optimization;

namespace InterZoo
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/Scripts/Load")
                .Include("~/js/load.js"));


            bundles.Add(new StyleBundle("~/Content/Zoo")
             .Include("~/css/bootstrap.css",
             "~/css/blast.min.css",
             "~/css/portfolio.css",
             "~/css/style.css",
             "~/css/font-awesome.min.css"
              ));

            bundles.Add(new ScriptBundle("~/Scripts/jquery")
             .Include("~/js/jquery-2.2.3.min.js"));

            bundles.Add(new ScriptBundle("~/Scripts/password")
            .Include("~/js/password.js"));


            bundles.Add(new ScriptBundle("~/Scripts/responsiveSlides")
            .Include("~/js/responsiveslides.min.js", 
            "~/js/responsiveSlides.js"
           ));

            bundles.Add(new ScriptBundle("~/Scripts/scrolling")
             .Include("~/js/scrolling-nav.js",
             "~/js/counter.js"
             ));

            bundles.Add(new ScriptBundle("~/Scripts/picEyes")
             .Include("~/js/jquery.picEyes.js", 
             "~/js/picEye.js"

             ));

            bundles.Add(new ScriptBundle("~/Scripts/smothScrolling")
           .Include("~/js/move-top.js",
           "~/js/easing.js",
           "~/js/scroll.js"
           ));

            bundles.Add(new ScriptBundle("~/Scripts/easeOutQuart")
        .Include("~/js/easeOutQuart.js",
        "~/js/SmoothScroll.min.js",
        "~/js/blast.min.js",
        "~/js/bootstrap.js"


        ));






        }
    }
}

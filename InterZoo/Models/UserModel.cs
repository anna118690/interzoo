﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterZoo.Models
{
    public class UserModel
    {
        private string _nom;
        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }
    }
}
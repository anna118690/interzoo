﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InterZoo.Models;
using InterZoo.Tools;
using InterZoo.Tools.Mappers;
using InterZooDAL.Repositories;
using InterZooDAL.Models;
using InterZoo.Areas.User.Models;

namespace InterZoo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();

        }


        [HttpGet]
        public ActionResult Login()
        {
            if (SessionUtils.IsConnected)
            {
                return RedirectToAction("Index", new { controller = "Home", area = "User" });
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel m)
        {
            //I intanciate a UserRepository with the connectionstring.
            // The connectionstring is stored into web.config and i get it with the ConfigurationManager
            // If you want more information about ConfigurationManger don't forget to use F12.

            UserRepository Mr = new UserRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);

            //I use the function VerifLogin from the MembreRepository
            //For use this function, I have to convert The LoginModel to the
            ProfileModel Mmodel = MapToDBModels.UserToProfile(Mr.VerifLogin(MapToDBModels.LoginToUser(m)));
            if (Mmodel != null)
            {
                SessionUtils.ConnectedUser = Mmodel;
                SessionUtils.IsConnected = true;
                return RedirectToAction("Index", new { controller = "Home", area = "User" });
            }
            else
            {
                ViewBag.ErrorLoginMessage = "Erreur Login/Mot de passe";
                return RedirectToAction("Index");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult Register(RegisterModel Rm)
        {

            //Check if data annotations are respected ==> See the RegisterModel Class
            if (!ModelState.IsValid)
            {
                //I want to get all error on the model like wrong email format, wrong password repetiton,....
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        //add the error message into a viewbag to display on the view
                        ViewBag.ErrorMessage += error.ErrorMessage + "<br>";
                        
                    }
                }
                return View("Index");
            }
            else
            {
                UserRepository Mr = new UserRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);
                //I have to call the Insert function from MembreRepository
                //If the insert succeed, we get a complete Member with id value (calculated by the database)
                //If the insert failed, we receive a null value
                //We have to convert the registerModel(viewmodel) to a MembreModel(Dal) before to call the function
                // this is why we call the static function RegisterToMembre from the Static lass MapToDBModel
                User M = Mr.Insert(MapToDBModels.RegisterToUser(Rm));
                if (M != null)
                {
                    return View("Index");

                }

                else
                {
                    //If there is an issuer, I want to dispay a message on the view.
                    //Thus I use Viewbag to send the message to the view
                    ViewBag.ErrorMessage = "Erreur lors de l'insertion";
                    return View("Index");
                }

            }
        }

        


        public RedirectToRouteResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index");
        }


    }


}
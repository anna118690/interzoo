﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterZoo.Areas.User.Models
{
    public class ProfileModel
    {
        private int _id;
        private string _nom;
        private string _email;
        private string _motDePasse;

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }
        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

       

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        public string MotDePasse
        {
            get
            {
                return _motDePasse;
            }

            set
            {
                _motDePasse = value;
            }
        }

    }
}
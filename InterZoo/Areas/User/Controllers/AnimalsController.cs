﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InterZoo.Areas.User.Models;
using InterZooDAL.Repositories;
using InterZoo.Tools;
using InterZoo.Tools.Filters;
using InterZoo.Tools.Mappers;


namespace InterZoo.Areas.User.Controllers
{
    public class AnimalsController : Controller
    {
        // GET: User/Animals
        public ActionResult Index()
        {
            ViewBag.Current = "Animals";
            AnimalRepository CR = new AnimalRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);
            List<AnimalModel> Lc = CR.GetAnimalFromUser(SessionUtils.ConnectedUser.Id).Select(c => MapToDBModels.AnimalToAnimalModel(c)).ToList();
            return View(Lc);
        }
    }
}
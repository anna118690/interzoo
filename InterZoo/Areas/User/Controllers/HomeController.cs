﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InterZoo.Tools;
using InterZoo.Tools.Filters;


namespace InterZoo.Areas.User.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!SessionUtils.IsConnected)
            {
                return RedirectToAction("Login", new { controller = "Home", area = "" });
            }
            else
            {
                ViewBag.Current = "";
                return View( SessionUtils.ConnectedUser);
            }


        }


    }
}

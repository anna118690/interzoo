﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InterZoo.Models;
using InterZoo.Areas.User.Models;
using InterZooDAL.Repositories;
using InterZooDAL.Models;

namespace InterZoo.Tools.Mappers
{
    public static class MapToDBModels
    {
        public static User LoginToUser(LoginModel lm)
        {
            return new User()
            {
                Email = lm.Email,
                MotDePasse = lm.Password
            };
        }


        public static UserModel UserToUserModel(User user)
        {
            if (user == null) return null;
            return new UserModel()
            {
                Nom = user.Nom
            };
        }


        public static AnimalModel AnimalToAnimalModel(Animal c)
        {
            return new AnimalModel()
            {
                Nom = c.Nom,
                IdAnimal = c.Id,
                DateDeNaissance = c.DateDeNaissance,
                DateDeDecede = c.DateDeDecede,
                IdRegion =c.IdRegion,
                IdEspece = c.IdEspece

            };
        }

        internal static ProfileModel UserToProfile(User aream)
        {
            if (aream == null) return null;

            return new ProfileModel()
            {
                Id = aream.Id,
                Nom = aream.Nom,
                Email = aream.Email
            };
        }

        public static User RegisterToUser(RegisterModel rm)
        {
            return new User()
            {
                Nom = rm.Nom,
                Email = rm.Email,
                MotDePasse = rm.MotDePasse
            };
        }

 

    }
}